package com.springboot.microservice.app.bootstrap;

import com.springboot.microservice.app.model.Address;
import com.springboot.microservice.app.repository.AddressRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AddressDataLoader implements CommandLineRunner {

    private final AddressRepository addressRepository;

    public AddressDataLoader(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Address address1 = Address.builder()
                .street("Mulberry Street")
                .houseNumber(2830)
                .zipcode(75904)
                .city("Lufkin")
                .state("TX")
                .build();
        addressRepository.save(address1);

        Address address2 = Address.builder()
                .street("John Calvin Drive")
                .houseNumber(1059)
                .zipcode(60409)
                .city("Calumet City")
                .state("IL")
                .build();
        addressRepository.save(address2);

        Address address3 = Address.builder()
                .street("Rodney Street")
                .houseNumber(4535 )
                .zipcode(62712)
                .city("SPRINGFIELD")
                .state("IL")
                .build();
        addressRepository.save(address3);

    }
}
