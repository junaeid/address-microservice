package com.springboot.microservice.app.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.springboot.microservice.app.dto.AddressRequest;
import com.springboot.microservice.app.dto.AddressResponse;
import com.springboot.microservice.app.exception.AddressAlreadyExistsException;
import com.springboot.microservice.app.service.AddressService;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/address")
@RestController
public class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping("{id}")
    public ResponseEntity<EntityModel<AddressResponse>> getAddressById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(EntityModel.of(addressService.getById(id),
                linkTo(methodOn(AddressController.class).getAddressById(id)).withSelfRel()
        ));
    }

    @PostMapping("create")
    public ResponseEntity addNewAddress(@RequestBody AddressRequest addressRequest) throws AddressAlreadyExistsException {
        //System.out.println(addressRequest.getStreet());
        AddressResponse savedAddress = addressService.createAddress(addressRequest);
        return new ResponseEntity(savedAddress, HttpStatus.CREATED);
    }
}
