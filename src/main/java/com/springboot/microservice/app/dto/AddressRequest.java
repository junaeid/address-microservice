package com.springboot.microservice.app.dto;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddressRequest extends RepresentationModel<AddressRequest> {

    private Long Id;
    private String street;
    private String city;
    private String state;
    private int zipcode;
    private int houseNumber;
}
