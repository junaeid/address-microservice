package com.springboot.microservice.app.dto;

import com.springboot.microservice.app.model.Address;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddressResponse extends RepresentationModel<AddressResponse> {
    private Long addressId;
    private String street;
    private String city;
    private String state;
    private int zipcode;
    private int houseNumber;
}
