package com.springboot.microservice.app.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddressAlreadyExistsException extends RuntimeException {
    @Value(value = "${data.exception.messageOne}")
    private String message;
}
