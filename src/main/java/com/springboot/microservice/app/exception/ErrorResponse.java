package com.springboot.microservice.app.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date timestamp = new Date();
    private int code;
    private String status;
    private String message;
    private String stackTrace;
    private Object data;

    public ErrorResponse(HttpStatus status, String message) {
        this.code = status.value();
        this.status = status.name();
        this.message = message;
    }

}
