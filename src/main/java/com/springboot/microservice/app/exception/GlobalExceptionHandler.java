package com.springboot.microservice.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = AddressAlreadyExistsException.class)
    public ResponseEntity<ErrorResponse> blogAlreadyExistsException(AddressAlreadyExistsException addressAlreadyExistsException) {
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.CONFLICT, addressAlreadyExistsException.getMessage()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = AddressNotFoundException.class)
    public ResponseEntity<ErrorResponse> blogNotFoundException(AddressNotFoundException addressNotFoundException) {
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.NOT_FOUND, addressNotFoundException.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> databaseConnectionFailsException(Exception exception) {
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,"Data Connectivity Lost"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
