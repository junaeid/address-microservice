package com.springboot.microservice.app.mapper;

import com.springboot.microservice.app.dto.AddressRequest;
import com.springboot.microservice.app.dto.AddressResponse;
import com.springboot.microservice.app.model.Address;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    List<AddressResponse> addressToAddressResponse(List<Address> addresses);

    List<Address> addressResponseToAddress(List<AddressResponse> addressResponses);

    AddressResponse addressToAddressResponse(Address address);

    Address addressResponseToAddress(AddressResponse addressResponse);

    AddressRequest addressToAddressRequest(Address address);

    Address addressRequestToAddress(AddressRequest addressRequest);
}
