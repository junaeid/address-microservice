package com.springboot.microservice.app.repository;

//import com.springboot.microservice.app.dto.response.AddressResponse;
import com.springboot.microservice.app.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
}
