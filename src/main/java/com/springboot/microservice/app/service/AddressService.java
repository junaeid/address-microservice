package com.springboot.microservice.app.service;

import com.springboot.microservice.app.dto.AddressRequest;
import com.springboot.microservice.app.dto.AddressResponse;
import com.springboot.microservice.app.exception.AddressAlreadyExistsException;
import com.springboot.microservice.app.exception.AddressNotFoundException;

public interface AddressService {

    AddressResponse createAddress(AddressRequest addressRequest) throws AddressNotFoundException;
    AddressResponse getById(Long id) throws AddressAlreadyExistsException;
}
