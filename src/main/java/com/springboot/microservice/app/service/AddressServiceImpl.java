package com.springboot.microservice.app.service;

import com.springboot.microservice.app.dto.AddressRequest;
import com.springboot.microservice.app.dto.AddressResponse;
import com.springboot.microservice.app.exception.AddressAlreadyExistsException;
import com.springboot.microservice.app.exception.AddressNotFoundException;
import com.springboot.microservice.app.mapper.AddressMapper;
import com.springboot.microservice.app.model.Address;
import com.springboot.microservice.app.repository.AddressRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;

    public AddressServiceImpl(
            AddressRepository addressRepository,
            AddressMapper addressMapper
    ) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
    }

    @Override
    public AddressResponse createAddress(AddressRequest addressRequest) throws AddressAlreadyExistsException {
        if (addressRepository.existsById(addressRequest.getId())) {
            throw new AddressAlreadyExistsException();
        }
        Address address = addressMapper.addressRequestToAddress(addressRequest);
        addressRepository.save(address);


        return addressToAddressResponse(address);
    }

    @Override
    public AddressResponse getById(Long id) {
        log.info("from getById " + id);
        Address address;
        if (addressRepository.findById(id).isEmpty()) {
            throw new AddressNotFoundException();
        } else {
            address = addressRepository.findById(id).get();
        }
        return addressToAddressResponse(address);
    }

    public AddressResponse addressToAddressResponse(Address address) {
        return addressMapper.addressToAddressResponse(address);
    }
}
